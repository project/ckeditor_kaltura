/**
 * @file
 */

(function ($) {

  'use strict';

  CKEDITOR.plugins.add('kaltura', {
    init: function (editor) {
      CKEDITOR.dialog.add(this.name, this.path + 'dialogs/kaltura.js');
      editor.addCommand(this.name, new CKEDITOR.dialogCommand(this.name));

      editor.ui.addButton('kaltura_button', {
        label: Drupal.t('Insert kaltura element'),
        command: 'kaltura',
        icon: this.path + 'images/kaltura.png'
      });
    },
    afterInit: function (editor) {
      var dataProcessor = editor.dataProcessor;
      var dataFilter = dataProcessor && dataProcessor.dataFilter;

      if (dataFilter) {
        dataFilter.addRules({
          text: function (text) {
            var pattern = /\[kaltura:embed:([^\[\]]*)\]/;

            // Replace our kaltur-token by a thumbnail.
            return text.replace(pattern, function (match, optionstring) {
              var options = optionstring.split(':');
              var settings = Drupal.settings.ckeditor_kaltura;
              var player_info = settings.player_info[options[1]];
              var width = (options[2] ? options[2] : player_info.width);
              var height = (options[3] ? options[3] : player_info.height);
              // Building the thumbnail URL.
              var thumbnail = settings.kaltura_server_url + '/p/' + settings.kaltura_partner_id;
              thumbnail += '/thumbnail/entry_id/' + options[0] + '/width/' + width;
              thumbnail += '/height/' + height;

              var realFragment = new CKEDITOR.htmlParser.fragment.fromHtml(match);
              var realElement = realFragment && realFragment.children[0];

              // Creating a fake element so our token can be used in the
              // plain text mode.
              realElement.attributes = {};
              var tmp = editor.createFakeParserElement(realElement, 'cke_kaltura', 'string', true);
              tmp.attributes.src = thumbnail;
              tmp.attributes.width = width;

              var writer = new CKEDITOR.htmlParser.basicWriter();

              tmp.writeHtml(writer);

              return writer.getHtml();
            });
          }
        });
      }
    }
  });
})(jQuery);
