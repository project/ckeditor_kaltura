/**
 * @file
 * Defining the kaltura dialog.
 */

(function ($) {

  'use strict';

  /**
   * Add the kaltura dialog.
   */
  CKEDITOR.dialog.add('kaltura', function (editor) {
    function browseServer(evt) {
      var ckeditor = this;

      var modal_settings = {
        modalTheme: 'CToolsModalDialog',
        title: 'CKEditor Kaltura'
      };
      Drupal.CTools.Modal.show(modal_settings);
      // Change the z-index of modal because CKEditor modal has an z-index of
      // 10000.
      $('#modalContent').css('z-index', 11000);

      // "this" is now a CKEDITOR.dialog object.
      var ajax_settings = {};
      ajax_settings.event = 'KalturaCKEditorDialog';
      ajax_settings.url = Drupal.settings.basePath + 'ckeditor_kaltura_list_items';
      ajax_settings.progress = {
        type: 'throbber',
        message: Drupal.t('Loading Dialog...')
      };

      Drupal.ajax['kaltura-ckeditor-dialog'] = new Drupal.ajax('modalContent', $('#modalContent'), ajax_settings);

      // @TODO: Jquery 1.5 accept success setting to be an array of functions.
      // But we have to wait for jquery to get updated in Drupal core.
      // In the meantime we have to override it.
      Drupal.ajax['kaltura-ckeditor-dialog'].options.success = function (response, status) {
        if (typeof response == 'string') {
          response = $.parseJSON(response);
        }

        // Call the ajax success method.
        Drupal.ajax['kaltura-ckeditor-dialog'].success(response, status);

        Drupal.attachBehaviors('#modalContent');

        var $modal_wrapper = $('#modalContent');
        var dialog = ckeditor.getDialog();
        var editor = dialog.getParentEditor();
        editor._.filebrowserSe = ckeditor;

        // Watch for clicking the 'Insert' button.
        $modal_wrapper.find('.kentry_add').bind('click', function (e) {
          e.preventDefault();
          var $link = $(this);
          var entry_id = $link.attr('id');

          // Setting the entry id to the dialog.
          dialog.getContentElement('info', 'entry_id').setValue(entry_id);
        });
      };

      $('#modalContent').trigger('KalturaCKEditorDialog');

      return;
    }

    var settings = Drupal.settings.ckeditor_kaltura;
    var players = settings.players;
    return {
      title: 'Kaltura',
      minWidth: 450,
      minHeight: 260,
      contents: [{
        id: 'info',
        label: '',
        title: '',
        expand: true,
        padding: 0,
        elements: [{
          type: 'vbox',
          widths: ['280px', '30px'],
          align: 'left',
          children: [{
            type: 'hbox',
            widths: ['280px', '30px'],
            align: 'left',
            children: [{
              type: 'text',
              id: 'entry_id',
              label: 'Entry ID:',
              setup: function (element) {
                // Inserting the entry_id in the dialog.
                this.setValue(element[0]);
              },
              validate: CKEDITOR.dialog.validate.notEmpty(Drupal.t('Entry ID can not be empty'))
            }, {
              type: 'button',
              id: 'browse',
              label: editor.lang.common.browseServer,
              style: 'display:inline-block;margin-top:13px;',
              onClick: browseServer
            }]
          }, {
            type: 'hbox',
            widths: ['280px', '30px'],
            align: 'left',
            children: [{
              type: 'select',
              id: 'player_id',
              default: null,
              label: 'Player',
              items: players,
              setup: function (element) {
                // Inserting the player_id in the dialog.
                this.setValue(element[1]);
              },
              validate: CKEDITOR.dialog.validate.notEmpty(Drupal.t('Player ID can not be empty'))
            }]
          }, {
            type: 'hbox',
            widths: ['280px', '30px'],
            align: 'left',
            children: [{
              type: 'text',
              id: 'width',
              label: 'Width:',
              default: null,
              setup: function (element) {
                // Inserting the player_id in the dialog.
                this.setValue(element[2]);
              }
            }]
          }, {
            type: 'hbox',
            widths: ['280px', '30px'],
            align: 'left',
            children: [{
              type: 'text',
              id: 'height',
              label: 'Height:',
              default: null,
              setup: function (element) {
                // Inserting the player_id in the dialog.
                this.setValue(element[3]);
              }
            }]
          }]
        }]
      }],

      buttons: [CKEDITOR.dialog.okButton, CKEDITOR.dialog.cancelButton],

      /**
       * Called when the dialog is closed by pressing the 'ok' button.
       */
      onOk: function () {
        var currentDialog = CKEDITOR.dialog.getCurrent();
        var entry_id = currentDialog.getContentElement('info', 'entry_id').getValue();
        var player_id = currentDialog.getContentElement('info', 'player_id').getValue();
        var width = currentDialog.getContentElement('info', 'width').getValue();
        var height = currentDialog.getContentElement('info', 'height').getValue();

        var token = '[kaltura:embed:' + entry_id + ':' + player_id;
        if (width) {
          token += ':' + width;
        }
        if (height) {
          token += ':' + height;
        }
        token += ']';

        editor.insertText(token);
      },

      /**
       * The code that will be executed when a dialog window is loaded.
       *
       * @see http://docs.ckeditor.com/#!/guide/plugin_sdk_sample_2
       */
      onShow: function () {
        this.fakeImage = this.element = null;
        var fakeImage = this.getSelectedElement();

        // Check if a fake element is selected.
        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'string') {
          this.fakeImage = fakeImage;

          // Decoding the fake element. This is required because the
          // kaltura token is not a real html object so it can't be decoded
          // by any CKEditor or Drupal function.
          var element = decodeURIComponent(fakeImage.data('cke-realelement'));
          var pattern = /\[kaltura:embed:([^\[\]]*)\]/;
          var args = element.match(pattern);
          args = args[1].split(':');

          // Setup all the data from the token.
          this.setupContent(args);
          this.insertMode = false;
        }
        else {
          this.insertMode = true;

        }
      }
    };
  });

})(jQuery);
