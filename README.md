CKEditor Kaltura (ckeditor_kaltura)
===========
Thanks for downloading CKEditor Kaltura! This module provides a powerful
connection between CKEditor and Kaltura.

Requirements
-------------
This module requires the following modules:
* [CKEditor](http://drupal.org/project/ckeditor)
* [Kaltura](http://drupal.org/project/kaltura)
  * Field Kaltura
* [Token](http://drupal.org/project/token)
* [Token Filter](http://drupal.org/project/token_filter)

Installation
------------

1. Download and install the following modules, if these modules are not
installed on your Drupal Website:
  * CKEditor
  * Kaltura
  * Field Kaltura
  * Token
  * Token Filter
2. Place these module directories in your modules folder (this will usually be
   "sites/all/modules/").
3. Enable the CKEditor Kaltura module. Drupal will install all dependencies
automatically.

Configuration
-------------
1. Edit the CKEditor profile you wish to add the CKEditor plugin at
`admin/config/content/ckeditor`.
2. Expand the 'Edit Appearance` fieldset and navigate to '**Plugins**'.
3. Enable '*Plugin for Kaltura in CKEditor*'.
4. Scroll up to the '**Toolbar**' section and drag the Kaltura Icon into the
tooltbar and click Save.
5. Enable (if you haven't done yet) the Token Filter at all text formats you
want to at `admin/config/content/formats`.
6. CKEditor will be cached by your webbrowser, so empty your browser caches to
make sure you can use the CKEditor Plugin
