/**
 * @file
 * Attaches the behaviors for CKEditor Kaltura.
 */

(function ($) {

  'use strict';

  /**
   * Open the video player.
   *
   * This could override a field_kaltura behavior, because we don't want to embed
   * the video twice.
   */
  Drupal.behaviors.fieldKalturaEmbed = {
    attach: function (context, settings) {
      settings.kaltura = settings.kaltura || {};
      settings.kaltura.embedKWidget = settings.kaltura.embedKWidget || {};

      $.each(settings.kaltura.embedKWidget, function (id, vars) {
        $('#' + id, context).once('kaltura-embed', function () {
          kWidget.embed(vars);
        });
      });
    }
  };

})(jQuery);
